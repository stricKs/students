# Students

This project was generated with Angular CLI version 6.2.5.
Node version: 10.15.3
npm version: 6.4.1


## Getting started

Clone repo with `git clone https://stricKs@bitbucket.org/stricKs/students.git`

## Development server

run `npm install`
run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

For login use any username and password.

## Build

run `npm install`
run `ng build` for development build.
run `npm run build:prod` for production build.

The build artifacts will be stored in the `dist/` directory. 

## Running unit tests

Run `ng test` to execute the unit tests.


