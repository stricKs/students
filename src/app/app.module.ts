import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";

import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { StudentService } from './services/student.service';
import { CourseService } from './services/course.service';

import { AppComponent } from './app.component';
import { navigatableComponents } from "./app-routing.module";

import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { MessageModule } from "primeng/message";
import { MessagesModule } from 'primeng/messages';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MultiSelectModule } from 'primeng/multiselect';
import { HeaderComponent } from './components/header/header.component';



@NgModule({
  declarations: [
    AppComponent,
    ...navigatableComponents,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    InputTextModule,
    PasswordModule,
    ButtonModule,
    MessageModule,
    MessagesModule,
    PaginatorModule,
    TableModule,
    ToastModule,
    MultiSelectModule
    
  ],
  providers: [
    AuthGuard,
    AuthService,
    StudentService,
    CourseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

