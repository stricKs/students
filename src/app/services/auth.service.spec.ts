import { Login } from './../classes/login';
import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { Router } from '@angular/router';

describe('AuthService', () => {

  let routerStub: Partial<Router>

  beforeEach(() => TestBed.configureTestingModule({
    providers: [ 
      { provide: Router, useValue: routerStub }
    ]
  }));

  it('should login a user', (done: DoneFn) => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
    let loginData: Login = new Login("name1", "password1");
    service.login(loginData).subscribe(res => {
      expect(service.isLoggedIn).toBe(true);
      expect(res.token).toBe(loginData.username + "Token" + loginData.password);
      done();
    });
  });
});
