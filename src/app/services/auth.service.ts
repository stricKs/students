import { Injectable } from '@angular/core';
import { Login } from '../classes/login';
import { of, Observable } from 'rxjs';
import { delay, map } from "rxjs/operators"
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean = false;

  constructor(private router: Router) { }

  login(loginData: Login): Observable<any> {
    return of({token: loginData.username + "Token" + loginData.password}).pipe(
      delay(1000),
      map(res => {
        this.saveToken(res.token);
        this.isLoggedIn = true;
        return res;
      })
    );
  }

  saveToken(token: string): void {
    localStorage.setItem("accessToken", token);
  }

  logout(): void {
    this.isLoggedIn = false;
    localStorage.removeItem("accessToken");
    this.router.navigate(["login"]);
  }

  hasToken(): boolean {
    if (!localStorage.getItem("accessToken")) {
      return false;
    }
    this.isLoggedIn = true;
    return true;
  }

}
