import { CourseService } from './course.service';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { delay, map } from "rxjs/operators"
import { Router } from '@angular/router';
import { Paging } from '../classes/paging';
import { Student } from '../classes/student';
import { Course } from '../classes/course';

import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  students: Student[] = [];

  constructor() {

    let i;
    for(i = 0; i < 19; i++) {
      this.students.push(new Student({
        id: i,
        firstname: "studentName" + i,
        lastname: "studentLastname" + i,
        courses: [
          new Course({name: "Physics", credits: 10}),
          new Course({name: "Math", credits: 10})
        ]
      }));
    }
  }


  getStudents(paging: Paging): Observable<Student[]> {
    let startIndex: number = paging.page * paging.pageSize;
    let endIndex: number = startIndex + paging.pageSize;
    return of(this.students).pipe(
      delay(500),
      map(students => {
        return students.slice(startIndex, endIndex);
      })
    );
  }

  getTotalStudents(): Observable<number> {
    return of(this.students.length).pipe(
      delay(300)
    );
  }

  removeStudent(student: Student): Observable<Student> {
    _.remove(this.students, (s: Student) => s.id == student.id);
    return of(student).pipe(
      delay(300)
    );
  }

  getStudent(studentId: number): Observable<Student> {
    let student: Student = this.students.find(s => s.id == studentId);
    // if (!student) return null;
    return of(student).pipe(
      delay(200)
    );
  }

  updateStudentCourses(student: Student, courses: Course[]): Observable<Student> {
    let studentToUpdate: Student = this.students.find(s => s.id == student.id);
    studentToUpdate.courses = courses.map(c => new Course(c));
    return of(studentToUpdate);
  }

  addStudent(student: Student): Observable<Student> {
    let lastStudent: Student = _.last(this.students);
    let newId: number = 0;
    if (lastStudent) {
      newId = lastStudent.id + 1;
    }
    student.id = newId;
    this.students.push(student);
    return of(student).pipe(delay(400));
  }

}
