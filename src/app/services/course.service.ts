import { Course } from './../classes/course';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {


  constructor() { }

  getCourses(): Observable<Course[]> {
    return of([
      new Course({ name: "Physics", credits: 10 }),
      new Course({ name: "Math", credits: 10 }),
      new Course({ name: "Biology", credits: 6 }),
      new Course({ name: "Chemistry", credits: 8 }),
      new Course({ name: "History", credits: 4 }),
      new Course({ name: "Chess", credits: 2 })
    ]).pipe(
      delay(300)
    );
  }

}
