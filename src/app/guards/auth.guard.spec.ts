import { AuthService } from './../services/auth.service';
import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { Router } from '@angular/router';

describe('AuthGuard', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Router, AuthGuard, AuthService]
    });
  });

  // it('should not let user see guarded components', 
  //     inject([AuthGuard, AuthService, Router], 
  //     (guard: AuthGuard, auth: AuthService, router: Router) => {
        
  //   console.log("Asdf", auth.isLoggedIn);
  //   expect(auth.isLoggedIn).toBe(true);
  // }));
});
