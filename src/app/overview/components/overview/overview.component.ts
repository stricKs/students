import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../../services/student.service';
import { Paging } from '../../../classes/paging';
import { Student } from '../../../classes/student';
import { PaginatorChange } from '../../../classes/paginatorChange';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  providers: [ MessageService ]
})
export class OverviewComponent implements OnInit {


  students: Student[] = [];
  totalStudents: number = 0;
  paging: Paging = new Paging();

  activeTab: number = 0;

  constructor(private studentService: StudentService,
              private messageService: MessageService) { }

  ngOnInit() {
    this.refreshStudents();
  }

  refreshStudents(): void {
    this.getStudents();
    this.getTotalStudents();
  }

  getStudents(): void {
    this.studentService.getStudents(this.paging).subscribe(students => {
      this.students = students;
    });
  }

  getTotalStudents(): void {
    this.studentService.getTotalStudents().subscribe(count => {
      this.totalStudents = count;
    });
  }

  changePage(paginatorChange: PaginatorChange): void {
    this.paging.page = paginatorChange.page;
    this.getStudents();
  }

  studentAdded(): void {
    this.messageService.add({severity:'success', summary: 'Student added', detail:'New student was added successfully.'});
    this.refreshStudents();
    this.activeTab = 0;
  }
  

}
