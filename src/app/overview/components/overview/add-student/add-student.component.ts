import { Student } from './../../../../classes/student';
import { CourseOption } from './../../../../classes/courseOption';
import { Course } from './../../../../classes/course';
import { CourseService } from './../../../../services/course.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StudentService } from '../../../../services/student.service';

@Component({
  selector: 'add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  @Output() studentAdded: EventEmitter<void>;

  studentForm: FormGroup = this.fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    courses: [[]]
  });

  courseOptions: CourseOption[] = [];

  constructor(private fb: FormBuilder,
              private courseService: CourseService,
              private studentService: StudentService) { 

    this.studentAdded = new EventEmitter();
  }

  ngOnInit() {
    this.courseService.getCourses().subscribe(courses => {
      this.courseOptions = courses.map(c => new CourseOption(c));
    });
  }

  isFieldInvalid(field: string): boolean {
    return this.studentForm.controls[field].dirty && !this.studentForm.controls[field].valid;
  }

  addStudent(): void {
    let student: Student = new Student(this.studentForm.value);
    this.studentService.addStudent(student).subscribe(res => {
      this.studentAdded.emit();
    });
  }

}
