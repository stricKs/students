import { StudentService } from './../../../../services/student.service';
import { MessageService } from 'primeng/api';
import { Student } from './../../../../classes/student';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'students-table',
  templateUrl: './students-table.component.html',
  styleUrls: ['./students-table.component.css'],
  providers: [ MessageService ]
})
export class StudentsTableComponent implements OnInit {

  @Input() students: Student[];
  @Output() updateStudents: EventEmitter<void>;


  studentToRemove: Student = null;

  constructor(private messageService: MessageService,
              private studentService: StudentService,
              private router: Router) { 

    this.updateStudents = new EventEmitter();
  }

  ngOnInit() {
  }

  removeStudent(student: Student): void {
    this.messageService.clear();
    this.messageService.add({
      key: 'c',
      sticky: true,
      severity: 'warn',
      summary: 'Are you sure?',
      detail: `You are about to delete a student ${student.firstname} ${student.lastname}`
    });
    this.studentToRemove = student;
  }

  onConfirm() {
    this.messageService.clear('c');
    this.studentService.removeStudent(this.studentToRemove).subscribe(res => {
      this.studentToRemove = null;
      this.updateStudents.emit();
    });
  }

  onReject() {
    this.studentToRemove = null;
    this.messageService.clear('c');
  }

}
