import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { OverviewRoutingModule, navigatableComponents } from './overview-routing.module';

import { StudentsTableComponent } from './components/overview/students-table/students-table.component';
import { AddStudentComponent } from './components/overview/add-student/add-student.component';

import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { MessageModule } from "primeng/message";
import { MessagesModule } from 'primeng/messages';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MultiSelectModule } from 'primeng/multiselect';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OverviewRoutingModule,
    InputTextModule,
    PasswordModule,
    ButtonModule,
    MessageModule,
    MessagesModule,
    PaginatorModule,
    TableModule,
    ToastModule,
    MultiSelectModule,
  ],
  declarations: [
    ...navigatableComponents,
    StudentsTableComponent,
    AddStudentComponent
  ]
})
export class OverviewModule { }
