import { AuthGuard } from './../guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from './components/overview/overview.component';


const routes: Routes = [
  { path: "", component: OverviewComponent, canActivate: [ AuthGuard ] }
];

export const navigatableComponents = [
  OverviewComponent
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverviewRoutingModule { }
