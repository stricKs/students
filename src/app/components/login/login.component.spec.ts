import { AuthService } from './../../services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';

import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { MessageModule } from "primeng/message";
import { MessagesModule } from 'primeng/messages';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MultiSelectModule } from 'primeng/multiselect';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  
  let routerStub: Partial<Router>

  beforeEach(async(() => {

    let loginRes = {};

    // Create a fake TwainService object with a `getQuote()` spy
    const authService = jasmine.createSpyObj('authService', ['login']);
    // Make the spy return a synchronous Observable with the test data
    let loginSpy = authService.login.and.returnValue( of(loginRes) );

    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        ReactiveFormsModule,
        InputTextModule,
        PasswordModule,
        ButtonModule,
        MessageModule,
        MessagesModule,
        PaginatorModule,
        TableModule,
        ToastModule,
        MultiSelectModule 
      ],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: Router, useValue: routerStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should login user", async() => {
    expect(component.loginForm.value.username).toBe("");
    expect(component.loginForm.value.password).toBe("");
    component.loginForm.controls.username.setValue("name");
    component.loginForm.controls.username.setValue("password");
    component.login();
    
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
