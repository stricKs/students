import { Login } from './../../classes/login';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    username: ["", Validators.required],
    password: ["", Validators.required]
  });

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
  }

  login(): void {
    let loginData: Login = new Login(this.loginForm.value.username, this.loginForm.value.password);
    this.authService.login(loginData).subscribe(res => {
      this.router.navigate(["overview"]);
    });
  }

  isFieldInvalid(field: string): boolean {
    return this.loginForm.controls[field].dirty && !this.loginForm.controls[field].valid;
  }

}
