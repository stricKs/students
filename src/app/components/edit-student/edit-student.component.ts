import { Course } from './../../classes/course';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../../services/student.service';
import { Student } from '../../classes/student';
import { CourseService } from '../../services/course.service';
import { CourseOption } from '../../classes/courseOption';

import * as _ from "lodash";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css'],
  providers: [ MessageService ]
})
export class EditStudentComponent implements OnInit {

  studentId: number;
  student: Student;
  courseOptions: CourseOption[] = [];

  selectedCourses: Course[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private studentService: StudentService,
              private courseService: CourseService,
              private messageService: MessageService) { 

    this.student = new Student();
  }

  ngOnInit() {
    
    this.studentId = +this.route.snapshot.params.studentId;
    this.studentService.getStudent(this.studentId).subscribe(student => {
      this.student = student;
      this.selectedCourses = student.courses.map(c => new Course(c));
    });
    this.courseService.getCourses().subscribe(courses => {
      this.courseOptions = courses.map(c => new CourseOption(c));
    });
  }

  updateCourses(): void {
    this.studentService.updateStudentCourses(this.student, this.selectedCourses).subscribe(res => {
      this.messageService.add({severity:'success', summary: 'Success', detail: 'Student was successfully updated.'});
      this.student = res;
      this.selectedCourses = this.student.courses.map(c => new Course(c));
    });
  }

  goBack() {
    this.router.navigate(["overview"]);
  }

}
