import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { EditStudentComponent } from './components/edit-student/edit-student.component';

 
const appRoutes: Routes = [
  { path: '', redirectTo: "/overview", pathMatch: 'full'},
  { path: 'students/:studentId', component: EditStudentComponent, canActivate: [ AuthGuard ] },
  { path: 'login', component: LoginComponent },
  { path: 'overview', loadChildren: './overview/overview.module#OverviewModule', canLoad: [ AuthGuard ] }

];

export const navigatableComponents = [
  LoginComponent,
  EditStudentComponent
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}