

export class PaginatorChange {

  page: number;
  first: number;
  rows: number;
  pageCount: number;

}