

export class Paging {

  page: number = 0;
  pageSize: number = 20;

  constructor(data?: any) {
    if (!data) return;
    this.page = data.page;
    this.pageSize = data.pageSize;
  }

}