import { Course } from "./course";

import * as _ from "lodash";

export class Student {

  id: number;
  firstname: string = "";
  lastname: string = "";
  courses: Course[] = [];

  constructor(data?: any) {
    if (!data) return;
    this.id = data.id;
    this.firstname = data.firstname;
    this.lastname = data.lastname;
    this.courses = _.map(data.courses, c => new Course(c));
  }

  get getCourses(): string {
    let courses: string[] = this.courses.map(course => course.name);
    return courses.join(", ");
  }
  
}