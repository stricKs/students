

export class Course {

  name: string;
  credits: number;

  constructor(data?: any) {
    if (!data) return;
    this.name = data.name;
    this.credits = data.credits;
  }

}