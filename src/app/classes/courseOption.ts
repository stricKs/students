import { Course } from './course';


export class CourseOption {

  label: string;
  value: Course;

  constructor(course: Course) {
    this.label = course.name;
    this.value = new Course(course);
  }

}